#include <Arduino.h>
#include <BluetoothSerial.h>

/****************************************************/
/****                DEFINES                     ****/
/****************************************************/
// PINS FOR PWM
#define PIN_LED_R         2
#define PIN_LED_G         4
#define PIN_LED_B         5

// ANALOG INPUTS
#define PIN_LIGHT_SENSOR  34
#define PIN_POTMETER      35

// BUTTON
#define PIN_BUTTON        32

// STATUS LEDS
#define PIN_MODE_LED_1    12
#define PIN_MODE_LED_2    13
#define PIN_MODE_LED_3    14

// BLUETOOTH
#define BLUETOOTH_NAME    "ESP32 - Project"
#define BUFFER_SIZE       256

// TIMER
#define TIMER_SPEED 5000	// 1 means 1us, 1000 = 1 milisecond, 1.000.000 = 1 second

/****************************************************/
/****               VARIABLES                    ****/
/****************************************************/
// PWM VARIABLES
const uint8_t PwmChannelRed     = 0;
const uint8_t PwmChannelGreen   = 1;
const uint8_t PwmChannelBlue    = 2;
const double PwmFrequency       = 5000;
const uint8_t PwmResolution     = 8;
volatile uint8_t PwmRed         = 255;
volatile uint8_t PwmGreen       = 255;
volatile uint8_t PwmBlue        = 255;

// BLUETOOTH VARIABLES
BluetoothSerial Bt;
char buffer[BUFFER_SIZE];

// ANALOG INPUT VARIABLES
uint16_t LightValue             = 0;
uint16_t PotmeterValue          = 0;

// TIMER
hw_timer_t * timer              = NULL;

// PROGRAM VARIABLES
volatile uint8_t ProgramMode    = 0;
int8_t increment                = 1;
uint8_t dataArrived             = 0;
uint8_t MutexProcess            = 0;

/****************************************************/
/****           FUNCTION PROTOTYPES              ****/
/****************************************************/
// PWM
void initPWM();
void SetPwmValues();

// BLUETOOT
void initBluetooth();
void bluetoothEventHandler(esp_spp_cb_event_t event, esp_spp_cb_param_t *param);
void clearBuffer();

// INPUTS OUTPUTS
void getAnalogInputs();
void initInputsOutputs();
void ButtonEventHandler();

//TIMER
void initTimer();
void IRAM_ATTR timerEventHandler();

// PROGRAM PROTOTYPES
void CommandHandler();
void switchMode();

// Setup function, called once at the beggining of the program
void setup() {
  // Calling all the init functions
  Serial.begin(9600);
  initBluetooth();
  initPWM();
  initInputsOutputs();
  initTimer();
}

// The main loop of the program
void loop() {
  getAnalogInputs();
  CommandHandler();
}


/****************************************************/
/****           FUNCTION DEFINITIONS             ****/
/****************************************************/

/**** BLUETOOTH *****/
void initBluetooth() {
  Serial.println("Init Bluetooth...");
  
  // TODO: Connect the Bluetooth callback event to the bluetooth event handler function
 
  // TODO: Call the begin function with the bluetooth name argument. Check if it was succesfully initilaized.
}

void bluetoothEventHandler(esp_spp_cb_event_t event, esp_spp_cb_param_t *param) {
// When connected.
  if(event == ESP_SPP_SRV_OPEN_EVT){
    Serial.println("Client Connected");
  }

  if(event == ESP_SPP_DATA_IND_EVT && dataArrived == 0 && MutexProcess == 0) {
    int data = '\0';
    uint8_t counter = 0;
    MutexProcess   = 1;
    while (1)
    {
      data = Bt.read();

      if(data == -1)
        return;

      // If it reached the buffers size place an end character at the end, and returns.
      if(counter == 255) {
        buffer[255] = '\0';
        dataArrived = 1;
        return;
      }

      // When the new line character arrives it is the end of the datastream.
      if(data == '\n') {
        buffer[counter] = '\0';
        dataArrived = 1;
        return;
      } else {
        buffer[counter] = data;
      }
      counter++;
    }
  }
}

void clearBuffer() {
  // TODO: Go through on the buffer array with a for loop, and place '\0' value in every place of the array
}

/**** PWM ****/
void initPWM() {
  Serial.println("Init PWM...");
  // TODO: Use ledSetup function for initialize the PWM channels.

  // TODO: Attach the R, G, B Pins to their PWM channels

  // TODO: As default set all PWM channels to max resolution, now it is 255
  // (255 means 0 light and 0 means max light)

  Serial.println("Init PWM done");
}

void SetPwmValues() {
  // If the MutexProcess   is 1 than return from the function

  // Use switch or if () else if() on ProgramMode
  // Depending on the mode do the following: 
  // ProgramMode = 1: increment the RED Pwm by one. Set others to 255
  //                  if it reachies 255 change the direction and decrement it
  //                  if it reaches 0 change the change the direction and increment it again.
  // ProgramMode = 2: increment the GREEN Pwm by one. Set others to 255
  //                  if it reachies 255 change the direction and decrement it
  //                  if it reaches 0 change the change the direction and increment it again.
  // ProgramMode = 3: increment the BLUE Pwm by one. Set others to 255
  //                  if it reachies 255 change the direction and decrement it
  //                  if it reaches 0 change the change the direction and increment it again.
}

/**** INPUT OUTPUT ****/
void initInputsOutputs() {
  Serial.println("Init inputs, outputs...");
  // Init MODE_LEDS as outputs
  // Init Button as Input_PULLDOWN
  // Attach PIN_BUTTON to BUttonEventHandler when the signal is RISING
  // SET MODE_LEDS to LOW
  Serial.println("Init inputs, outputs done");
}

void getAnalogInputs() {
  // Read analog values to LightValue and PotmeterValue
}

void ButtonEventHandler() {
  MutexProcess   = 1;
  // Increment ProgramMode by one
  // If the programMode reachies 4 set it to 0
  // Call swithcMode
  MutexProcess   = 0;
}

/**** TIMER ****/
void initTimer() {
  timer = timerBegin(0, 80, true);
  timerAttachInterrupt(timer, timerEventHandler, true);
  timerAlarmWrite(timer, TIMER_SPEED, true);
  timerAlarmEnable(timer);
}

void IRAM_ATTR timerEventHandler() {
  SetPwmValues();
}

/**** PROGRAM FUNCTIONS ****/
void switchMode() {
  // Set all PWM values to 255
  // Set increment to 1
  // Write a big swithc() statement. The argument of switch is ProgramMode
  // At case 0: All Mode Leds to LOW
  //            Set all PWM signals to 0V (255 duty cicle)
  //            Write to Serial, and send to Bluetooth: "Program mode: 0"
  // At case 1: MODE LED 1 HIGH others LOW
  //            Write to Serial, and send to Bluetooth: "Program mode: 1"
  // At case 2: MODE LED 2 HIGH others LOW
  //            Write to Serial, and send to Bluetooth: "Program mode: 2"
  // At case 3: MODE LED 3 HIGH others LOW
  //            Write to Serial, and send to Bluetooth: "Program mode: 3"
  // At defaul: set programMode to 0 and call swithcMode()
}

void CommandHandler() {
  // If dataArrived == 1 :  Set dataArrived to 0
  //                        Print out: "Message arrived: " + message
  //                        if the buffers first letter is C use switch on second letter. else send back the message to Bluetooth source 
  //                        Call clearBuffer() 
  //
  // Switch the second letter: case 'B' : Send the Brightness value to the bluetooth source
  //                           case 'P' : Send the Potentiometer value to the bluetooth source
  //                           case '0' : Print to serial: Command mode 0
  //                                      Set ProgramMode to 0
  //                                      Call switchMode()
  //                           case '1' : Print to serial: Command mode 1
  //                                      Set ProgramMode to 1
  //                                      Call switchMode()
  //                           case '2' : Print to serial: Command mode 2
  //                                      Set ProgramMode to 2
  //                                      Call switchMode()
  //                           case '3' : Print to serial: Command mode 3
  //                                      Set ProgramMode to 3
  //                                      Call switchMode()
  //                           default: break;
  //  At the end set MutexProcess to 0 because it is the end of the processing of the message s
}