# BLuetoothról és nyomógómbbal vezérelhető pulzáló rgb led

## Block diagramm  
![Diagram](img/bigtask.png)

## Circuit
![Circuit](img/circuit.png)

## Feladatleírás
* Lehessen az eszközhöz csatlakozni Bluetoothon
*  Az eszköznek 4 üzemmódja legyen:
1. Nem világít egyik led sem
2. A piros módjelző led világítson, és az RGB led pirosan pulzáljon
3. A zöld módjelző led világítson, és az RGB led zölden pulzáljon
4. A kék módjelző led világítson, és az RGB led kéken pulzáljon
* Bluetoothon küldendő parancsok. Az első karakter szerint:
1. N - rakja 1-es üzemmódba (nem világít semmi)
2. R - Rakja 2-es üzemmódba (piros)
3. G - Rakja 3-mas üzemmódba (zöld)
4. B - Rakja 4-es üzemmódba (kék)
5. P - Küldje el Bluetoothon a potméter értékét
* A nyomógomb benyomásával lehessen a 4 üzemmód között lépni
* Ahol szükséges ott a Serialra irassuk ki információkat.
* Ahol szükséges ott a Bluetoothra is kiirathatunk információkat
